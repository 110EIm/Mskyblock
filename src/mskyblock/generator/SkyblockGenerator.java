package mskyblock.generator;

import java.util.Map;

import cn.nukkit.Server;
import cn.nukkit.block.Block;
import cn.nukkit.block.BlockSapling;
import cn.nukkit.level.ChunkManager;
import cn.nukkit.level.format.generic.BaseFullChunk;
import cn.nukkit.level.generator.Generator;
import cn.nukkit.level.generator.object.tree.ObjectTree;
import cn.nukkit.math.NukkitRandom;
import cn.nukkit.math.Vector3;

public class SkyblockGenerator extends Generator {
	
	public static final int TYPE_SKYBLOCK = 3;
	
	private ChunkManager level;
	private NukkitRandom random;
	private Map<String, Object> options;

	
	public SkyblockGenerator(Map<String, Object> options) {
		this.options = options;
	}
	
	@Override
	public int getId() {
		return TYPE_SKYBLOCK;
	}

	@Override
	public void init(ChunkManager level, NukkitRandom random) {
		this.level = level;
		this.random = random;
	}

	@Override
	public void generateChunk(int chunkX, int chunkZ) {
		if (chunkX == 0 && chunkZ % 20 == 0) {
			BaseFullChunk chunk = level.getChunk(chunkX, chunkZ);
			for (int x = 0; x < 16; x++) {
				for (int z = 0; z < 16; z++) {
					chunk.setBlock(x, 0, z, Block.BEDROCK);
					for (int y = 1; y <= 3; y++) {
						chunk.setBlock(x, y, z, Block.STONE);
					}
					chunk.setBlock(x, 4, z, Block.DIRT);
					chunk.setBlock(x, 5, z, Block.GRASS);
					ObjectTree.growTree(level, chunkX*16 + 8, 6, chunkZ*16 + 8, random, BlockSapling.OAK);
				}
			}
		}
	}

	@Override
	public void populateChunk(int chunkX, int chunkZ) {
		//Do not write here anything.
	}

	@Override
	public Map<String, Object> getSettings() {
		return options;
	}

	@Override
	public String getName() {
		return "skyblock";
	}

	@Override
	public Vector3 getSpawn() {
		return new Vector3(0, 7, 1);
	}

	@Override
	public ChunkManager getChunkManager() {
		return level;
	}

}
